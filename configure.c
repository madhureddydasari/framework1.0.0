// This program creates and configures all necessary files and folders

#include "settings.cfg"
// #define DIR_NAME "domainName"
#define FILE_NAME_SIZE 100

// void createDir(char * dirname);
// void createPath();
void createConfigFiles();
void createNewEmptyConfigFile(char * fileName);


char * gPath;

// void main(int argc, char const *argv[])
// {
// 	createDir(DIR_NAME);
// 	// createPath();
// }


// ----------------------------------------------------------------------------------//
// void createPath() {
// 	gPath = (char *) malloc(FILE_NAME_SIZE * sizeof(char));
// 	sprintf(gPath, "%s%s", DIR_NAME, "\\\\");
// 	printf("%s\n", gPath);
// }

// void createDir(char * dirname) {
// 	int dirStatus = mkdir(DIR_NAME);
// 	if (!dirStatus) {
// 		createPath();
// 		createConfigFiles();
// 	}
// 	else {
// 		printf("Facing problem in creating new directory or else there is an existing directory with same name.\n");
// 	}
// }

void createConfigFiles() {
	createNewEmptyConfigFile(DATA_FILE);
	createNewEmptyConfigFile(MENU_FILE);
	createNewEmptyConfigFile(FIELDS_FILE);
	createNewEmptyConfigFile(TITLE_FILE);

}

void createNewEmptyConfigFile(char * fileName) {
	char *fileNameWithPath = (char*) malloc(FILE_NAME_SIZE * 5 * sizeof(char));
	sprintf(fileNameWithPath, "%s%s", gPath, fileName); 
	
	FILE *fp = fopen(fileNameWithPath, "w");
	if (fp == NULL) {
		printf("File %s is not created.\n", fileName);
	}
	else {
		printf("File %s created.\n", fileName);
	}
	fclose(fp);
}

