#include "stringDecorationFunctions.c"

#define HEADER_ROW_WIDTH (gFieldsCount) * FIELD_NAME_LENGTH + gFieldsCount + 1

char ** gNamesOfFields;
char * gHeader;

char ** gContentsOfMenu;
char * gMenu;

int gMenuCount;
int gFieldsCount;


void getFieldsCount();
void createHeaderContent();
void getMenuCount();
void createMenuContent();

char * getTitle();

char * returnHeader(); // operates on fields
int returnFieldCount();
char ** returnFields(); // operates on fields
bool returnSearchStatus(FILE *fpData);

void showHeader();
void showMenu();
void showFields(); 

void showTitle();

void initializeFunctions();
void create();
void showAll();
void search();
void update();
int delete();


void executeProgram();
void exitProgram();
void showRecord(char *record);







void getFieldsCount() {
	FILE *fpField;
	gFieldsCount = 0;
	char * fieldName = (char *) malloc(FIELD_NAME_LENGTH * sizeof(char));
	fpField  = fopen(FIELDS_FILE, "r");

	while(!feof(fpField)) {
		if(fgets(fieldName, FIELD_NAME_LENGTH, fpField) > 0) {
			gFieldsCount++;
		}
	}


	fclose(fpField);
}

void createHeaderContent() {
	int counter;
	gHeader = (char *) calloc(FIELD_NAME_LENGTH * gFieldsCount, sizeof(char));
	gNamesOfFields = (char **) malloc(gFieldsCount);

	for(counter = 0; counter < gFieldsCount; counter++) {
		gNamesOfFields[counter] = (char *) calloc(FIELD_NAME_LENGTH, sizeof(char));
	}

	FILE *fpField = fopen(FIELDS_FILE, "r");
	for(counter = 0; counter < gFieldsCount; counter++) {
		char * fieldValue = (char *) calloc(FIELD_NAME_LENGTH, sizeof(char));
		fgets(fieldValue, FIELD_NAME_LENGTH, fpField);
		fieldValue[strlen(fieldValue) - 1] = '\0';
		sprintf(gHeader, "%s%s", gHeader, returnCenterdTextOfTotalLength(fieldValue, FIELD_VALUE_LENGTH));
		strcpy(gNamesOfFields[counter], fieldValue);
		free(fieldValue);
	}
	sprintf(gHeader, "%s%s", gHeader, SEPARATOR_STYLE);
	fclose(fpField);
}

void getMenuCount() {
	FILE *fpMenu;
	gMenuCount = 0;
	char menuData[MENU_FIELD_NAME_LENGTH];
	fpMenu = fopen(MENU_FILE, "r");

	while(!feof(fpMenu)) {
		if(fgets(menuData, MENU_FIELD_NAME_LENGTH, fpMenu) > 0) {
			gMenuCount++;
		}
	}
	fclose(fpMenu);
}


void createMenuContent() {
	int counter;
	printf("gMenuCount %d", gMenuCount);
	gContentsOfMenu = (char **) calloc(gMenuCount + 10, sizeof(char));

	for(int i = 0; i < gMenuCount; i++) {
		gContentsOfMenu[i] = (char *) calloc(MENU_FIELD_NAME_LENGTH, sizeof(char));
	}

	FILE *fpMenu = fopen(MENU_FILE, "r");
	for(counter = 0; counter < gMenuCount; counter++) {
		char* menuFieldName = (char*)calloc(MENU_FIELD_NAME_LENGTH, sizeof(char));
		fgets(menuFieldName, MENU_FIELD_NAME_LENGTH, fpMenu);
		menuFieldName[strlen(menuFieldName) - 1] = '\0';
		strcpy(gContentsOfMenu[counter], menuFieldName);
		free(menuFieldName);
	}
	fclose(fpMenu);
}


char * getTitle() {
	FILE *fpTitle;
	fpTitle = fopen(TITLE_FILE, "r");

	char *title = (char*) calloc(TITLE_LENGTH, sizeof(char));
	char *titleContent = (char*) calloc(TITLE_LENGTH, sizeof(char));
	title[0] = '\0';
	titleContent[0] = '\0';

	while(!feof(fpTitle)) {
		if(fgets(titleContent, TITLE_LENGTH, fpTitle) > 0) {
			titleContent[strlen(titleContent) - 1] = '\0';
			strcat(title, titleContent);
		}
	}
	fclose(fpTitle);
	return title;
}

void showTitle() {
	char *title = (char*) calloc(TITLE_LENGTH, sizeof(char));
	title = getTitle();
	printf("\n");
	char formattedTitle[HEADER_ROW_WIDTH];

	printRepeatedTextCharacter(DECORATOR_STYLE, HEADER_ROW_WIDTH);
	sprintf(formattedTitle, "%s%s", returnCenterdTextOfTotalLength(title, HEADER_ROW_WIDTH - 2), SEPARATOR_STYLE);
	printf("\n%s\n", formattedTitle);
}


void showHeader() {
	char temporaryString[HEADER_ROW_WIDTH];

	showTitle();
	printRepeatedTextCharacter(DECORATOR_STYLE, HEADER_ROW_WIDTH);
	printf("\n");
	temporaryString[0] = '\0';
	// for(int i = 0; i < gFieldsCount; i++ ) {
	// 	sprintf(temporaryString,"%s%s", temporaryString, returnCenterdTextOfTotalLength(" ", FIELD_NAME_LENGTH));
	// }
	sprintf(temporaryString,"%s%s", temporaryString, returnCenterdTextOfTotalLength(" ", FIELD_NAME_LENGTH * gFieldsCount + gFieldsCount - 1));
	printf("%s%s", temporaryString, SEPARATOR_STYLE);
	printf("\n%s\n", gHeader);
	printf("%s%s\n", temporaryString, SEPARATOR_STYLE);
	printRepeatedTextCharacter(DECORATOR_STYLE, HEADER_ROW_WIDTH);
	printf("\n");
}


// void showMenu() {
// 	printf("\n");
// 	printf("\n");

// 	printCenteredUnderlinedTextOFTotalLength(MAIN_MENU_HEADING, HEADER_ROW_WIDTH);
// 	printf("\n");

// 	// char* gMenu = (char*) calloc(MENU_FIELD_NAME_LENGTH + 3, sizeof(char));
// 	gMenu = (char*) calloc(MENU_FIELD_NAME_LENGTH * gMenuCount, sizeof(char));
// 	for(int counter = 1; counter <= gMenuCount; counter++) {
// 		sprintf(gMenu, "%s%d%s%s\n", gMenu, counter, ". ", gContentsOfMenu[counter - 1]);
// 	}
// 	printf("%s", gMenu);
// 	printCenteredTextOFTotalLength("0. Exit", HEADER_ROW_WIDTH);
// 	// free(gMenu);
// }

void showMenu() {
	printf("\n");
	printf("\n");

	printCenteredUnderlinedTextOFTotalLength(MAIN_MENU_HEADING, HEADER_ROW_WIDTH);
	printf("\n");

	char* menuOption = (char*) calloc(MENU_FIELD_NAME_LENGTH + 3, sizeof(char));
	gMenu = (char*) calloc(MENU_FIELD_NAME_LENGTH * gMenuCount, sizeof(char));
	for(int counter = 1; counter <= gMenuCount; counter++) {
		sprintf(menuOption, "%s%d%s%s\n", menuOption, counter, ". ", gContentsOfMenu[counter - 1]);
	}
	printf("%s", menuOption);
	printCenteredTextOFTotalLength("0. Exit", HEADER_ROW_WIDTH);
	free(menuOption);
}





void executeProgram() {
	initializeFunctions();
	int option;
	while(1) {
		showMenu();
		printf("\nSelect valid option: ");
		fflush(stdin);
		scanf("%d", &option);

		switch(option) {
			case 1: create();
				    break;
			case 2: showAll();
				    break;
			case 3: search();
				    break;
			case 4: update();
				    break;
			case 5: delete();
				    break;
			case 0: exitProgram();
				    break;
			default: printf("Invalid option!");
				    break;
		}
	}
}

void initializeFunctions() {
	getFieldsCount();	// 1
	createHeaderContent();	// 2
	getMenuCount();		// 3
	createMenuContent();	// 4
}


// -----------------------------------------------------------------------------------------

void create() {
	FILE *fpData;
	fpData = fopen(DATA_FILE, "a");
	int counter;
	// char *record = (char*) calloc(FIELD_VALUE_LENGTH * gFieldsCount, sizeof(char));
	printf("Enter following details: \n");
	for(counter = 0; counter < gFieldsCount; counter++) {
		char *fieldValue = (char *) calloc(FIELD_VALUE_LENGTH, sizeof(char));

		if (counter == gFieldsCount - 1) {
			strcpy(fieldValue, ACTIVE);
		}
		else {
			printf("%s: ", gNamesOfFields[counter]);
			fflush(stdin);
			gets(fieldValue);
		}
		fieldValue[FIELD_VALUE_LENGTH - 1] = '\0';
		// printf("Field value %s, address is %d\n", fieldValue, fieldValue);
		if(fwrite(fieldValue, FIELD_VALUE_LENGTH, 1, fpData) <= 0) {
			printf("Write Unsuccessful.");
			break;
		}
		free(fieldValue);
	}
	fclose(fpData);
}

void showAll() {
	FILE *fpData;
	fpData = fopen(DATA_FILE, "r");
	int counter = 0;
	char *record = (char *) calloc(FIELD_VALUE_LENGTH * gFieldsCount, sizeof(char));
	showHeader();
	while(fread(record, FIELD_VALUE_LENGTH * gFieldsCount, 1, fpData) > 0) {
		showRecord(record);
	}
	fclose(fpData);
}

void search() {
	FILE *fpData;
	fpData = fopen(DATA_FILE, "r");

	bool searchStatus = false;
	char *record = (char *) calloc(FIELD_VALUE_LENGTH * gFieldsCount, sizeof(char));
	searchStatus = returnSearchStatus(fpData);
	if (searchStatus == true) {
		fseek(fpData, -1L * gFieldsCount * FIELD_VALUE_LENGTH, 1);
		showHeader();
		if (fread(record, FIELD_VALUE_LENGTH * gFieldsCount, 1, fpData) > 0) {
			showRecord(record);
		}
	}
	fclose(fpData);
}

bool returnSearchStatus(FILE *fpData) {
	char *record = (char*) calloc(FIELD_VALUE_LENGTH * gFieldsCount, sizeof(char));
	bool searchStatus = false;

	char primaryKey[FIELD_VALUE_LENGTH];
	printf("Enter %s: ", gNamesOfFields[0]);
	scanf("%s", primaryKey);

	while(fread(record, FIELD_VALUE_LENGTH * gFieldsCount, 1, fpData) > 0) {
			// printf("address of fpData inside while loop of returnStatus is %d\n", fpData);
		if (strcmp(primaryKey, record) == 0) {
			// printf("address of fpData inside while loop when MATCH FOUND of returnStatus is %d\n", fpData);
			searchStatus = true;
			printf("Match Found.");
			break;
		}
	}
	if (searchStatus == false) {
		printf("Match not Found.");
	} 
	return searchStatus;
}

void showFields() {
	printf("\n");
	printf("\n");

	printCenteredUnderlinedTextOFTotalLength(FIELDS_MENU_HEADING, HEADER_ROW_WIDTH);
	printf("\n");

	char* menuOption = (char*) calloc(FIELD_VALUE_LENGTH + 3, sizeof(char));
	for(int counter = 0; counter < gFieldsCount; counter++) {
		// gMenu = (char*) calloc(FIELD_VALUE_LENGTH * gMenuCount, sizeof(char));
		if (counter == 0) {
			continue;
		}
		sprintf(menuOption, "%s%d%s%s\n", menuOption, counter, ". ", gNamesOfFields[counter]);
	}

	printf("%s", menuOption);
	printCenteredTextOFTotalLength("0. Exit", HEADER_ROW_WIDTH);
	free(menuOption);
}

void showRecord(char *record) {
	char *formattedRecord = (char*) calloc(HEADER_ROW_WIDTH + gFieldsCount, sizeof(char));
	for(int counter = 0; counter < gFieldsCount; counter++) {
		char* fieldValue = (char *) calloc(FIELD_VALUE_LENGTH + 1, sizeof(char));
		strcpy(fieldValue, (record + FIELD_VALUE_LENGTH * counter));   // &a[i] = (a + i)
		sprintf(formattedRecord, "%s%s", formattedRecord, returnCenterdTextOfTotalLength(fieldValue, FIELD_VALUE_LENGTH));
	}
		sprintf(formattedRecord, "%s%s", formattedRecord, SEPARATOR_STYLE);
		printf("%s\n", formattedRecord);
		printRepeatedTextCharacter(DECORATOR_STYLE, HEADER_ROW_WIDTH);
		printf("\n");
}


void update()
{
	FILE *fpData;
	char *record, *primaryKey;
	fpData = fopen(DATA_FILE, "r+");
	int option;
	primaryKey = (char *) calloc(FIELD_VALUE_LENGTH, sizeof(char));
	printf("Enter %s: ", gNamesOfFields[0]);
	scanf("%s", primaryKey);
	bool searchStatus = false;

	while(!feof(fpData)) {
		record = (char *) calloc(FIELD_VALUE_LENGTH * gFieldsCount, sizeof(char));
		fread(record, FIELD_VALUE_LENGTH * gFieldsCount, 1, fpData);
													// OTHER WAY OF READING FILE CONTENTS AND PRINTING ON SCREEN.
		// for(int i = 0; i < 6; i++) {
		// 	printf("%s\n", &record[i * 20]);
		// }
		// printf("\n");
		// gets(primaryKey);
		if (strcmp(record, primaryKey) == 0) {
			searchStatus = true;
			showFields();
			printf("\nChoose valid option: ");
			scanf("%d", &option);

			if(option >= gFieldsCount){
				printf("Choose valid option.\n");
				break;
			}
			fseek(fpData, (-FIELD_VALUE_LENGTH * gFieldsCount * 1L) + (option * FIELD_VALUE_LENGTH * 1L), 1);
			printf("Enter %s: ", gNamesOfFields[option]);
			fflush(stdin);
			gets(record);
			record[FIELD_VALUE_LENGTH -1] = '\0';
			fwrite(record, FIELD_VALUE_LENGTH, 1, fpData);
			fclose(fpData);
			break;
		}
	}
	if (searchStatus == false) {
		printf("Match not found.\n");
	}
	free(record);  	
	fclose(fpData);
}

int delete() {
	FILE *fpData;
	char *statusFieldValue, *record, *primaryKey;
	fpData = fopen(DATA_FILE, "r+");
	// bool searchStatus = returnSearchStatus(fpData);

	primaryKey = (char *) calloc(FIELD_VALUE_LENGTH, sizeof(char));
	printf("Enter %s: ", gNamesOfFields[0]);
	scanf("%s", primaryKey);
	bool searchStatus = false;

	while(!feof(fpData)) {
		record = (char *) calloc(FIELD_VALUE_LENGTH * gFieldsCount, sizeof(char));
		fread(record, FIELD_VALUE_LENGTH * gFieldsCount, 1, fpData);

	// if (searchStatus == true) {
		if (strcmp(record, primaryKey) == 0) {
			searchStatus = true;
			int option;
			fseek(fpData, (-1 * FIELD_VALUE_LENGTH * 1L), 1);
			printf("\nTo delete, press 1");
			printf("\nTo go back, press any key");
			printf("\nEnter valid option: ");
			scanf("%d", &option);
			if (option == 1) {
				statusFieldValue = (char*) calloc(FIELD_VALUE_LENGTH, sizeof(char));
				strcpy(statusFieldValue, IN_ACTIVE);
				statusFieldValue[FIELD_VALUE_LENGTH - 1] = '\0';
				if(fwrite(statusFieldValue, FIELD_VALUE_LENGTH, 1, fpData) > 0) {
					printf("Record deletion successfully.");
				}
				else {
					printf("Record deletion Unsuccessful.");
				}
			}
			else {
				return 0;
			}
			break;
		}
		free(record);
	}
	if (searchStatus == false) {
		printf("Match Not Found");
	}
	fclose(fpData);
}



void exitProgram() {
	exit(0);
}
