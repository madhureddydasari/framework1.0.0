// #include <stdio.h>
// #include <string.h>
// #include <stdlib.h>
// #include <stdbool.h>
#include "settings.cfg"
#define SPACE_BEFORE_TEXT 10

void printHeadings();
void printRepeatedText(char *text, int numberOfTimes);
void printRepeatedTextCharacter(char *text, int numberOfTimes);
void printCenteredTextOFTotalLength(char *text, int totalLength);
void printCenteredText(char *text, int spaceCountAroundText);
void printUnderlinedText(char* text);
void printCenteredUnderlinedTextOFTotalLength(char* text, int totalLength);
void printCenteredUnderlinedText(char* text, int spaceCountAroundText);
char * returnCenterdTextOfTotalLength(char *text, int totalLength);
char * returnLeftSpaceAlignedTextOfTotalLength(char *text, int totalLength);

void printWelcomeTitle();

// -----------------------------------------------------------------------------------------------------------------------------//

void printWelcomeTitle() {
	printRepeatedTextCharacter(" ", WELCOME_MESSAGE_LENGTH);
	printRepeatedTextCharacter("#", strlen(WELCOME_MESSAGE));
	printf("\n");
	printCenteredUnderlinedText(WELCOME_MESSAGE, WELCOME_MESSAGE_LENGTH);
	printf("\n");
}

void printRepeatedText(char *text, int numberOfTimes) {
	char repeatedTextContainer[numberOfTimes + 1];
	for (int counter = 0; counter < numberOfTimes; counter++) {
		repeatedTextContainer[counter] = text[0];
	}
	repeatedTextContainer[numberOfTimes] = '\0';
	printf("\n%s\n", repeatedTextContainer);
}

void printRepeatedTextCharacter(char *text, int numberOfTimes) {
	char repeatedTextContainer[numberOfTimes + 1];
	for (int counter = 0; counter < numberOfTimes; counter++) {
		repeatedTextContainer[counter] = text[0];
	}
	repeatedTextContainer[numberOfTimes] = '\0';
	printf("%s", repeatedTextContainer);
}

char * returnCenterdTextOfTotalLength(char *text, int totalLength) {
	// char textToBeReturned[totalLength + 1];
	char *textToBeReturned = (char*) calloc(sizeof(char), totalLength + 2);
	strcat(textToBeReturned, SEPARATOR_STYLE);
	int spaceCountAroundText = (int) ((totalLength - strlen(text)) / 2);
	for(int counter = 0; counter < spaceCountAroundText; counter++) {
		strcat(textToBeReturned, " ");
	}
	strcat(textToBeReturned, text);
	for(int counter = 0; counter < totalLength - strlen(text) - spaceCountAroundText; counter++) {
		strcat(textToBeReturned, " ");
	}
	return textToBeReturned;
}

void printHeadings(int headerWidth) {
	char header[200];
	printRepeatedText("-", headerWidth);
	sprintf(header, "%s%s%s%s%s%s%s%s", returnCenterdTextOfTotalLength("Regd Number", HEADER_COLUMN_WIDTH), returnCenterdTextOfTotalLength("Name", HEADER_COLUMN_WIDTH), returnCenterdTextOfTotalLength("D.O.B.", HEADER_COLUMN_WIDTH), returnCenterdTextOfTotalLength("Student Phone", HEADER_COLUMN_WIDTH), returnCenterdTextOfTotalLength("Parent Phone", HEADER_COLUMN_WIDTH), returnCenterdTextOfTotalLength("Blood Group", HEADER_COLUMN_WIDTH), returnCenterdTextOfTotalLength("Status", HEADER_COLUMN_WIDTH), "|" );
	printf("%s", header);
	printRepeatedText("-", headerWidth);
	// printf("%s", returnCenterdTextOfTotalLength("menu", 15));
}

void printCenteredTextOFTotalLength(char *text, int totalLength) {

	int spaceCountAroundText = (int) ((totalLength - strlen(text)) / 2);
	for(int counter = 0; counter < spaceCountAroundText; counter++) {
		printf(" ");
	}
	printf("%s", text);
	for(int counter = 0; counter < spaceCountAroundText; counter++) {
		printf(" ");
	}
}

void printCenteredUnderlinedTextOFTotalLength(char* text, int totalLength) {
	printCenteredTextOFTotalLength(text, totalLength);
	printf("\n");
	char uderlineContainer[strlen(text)+1];
	for (int counter = 0; counter < strlen(text); counter++) {
		uderlineContainer[counter] = '-';
	}
	uderlineContainer[strlen(text)] = '\0';
	printCenteredTextOFTotalLength(uderlineContainer, totalLength);	
}

void printCenteredText(char *text, int spaceCountAroundText) {
	for(int counter = 0; counter < spaceCountAroundText; counter++) {
		printf(" ");
	}
	printf("%s", text);
	for(int counter = 0; counter < spaceCountAroundText; counter++) {
		printf(" ");
	}
}

void printUnderlinedText(char* text) {
	int lengthOfSequence = printf("%s",text);
	printf("\n");
	for(int i = 0; i < lengthOfSequence; i++) {
		printf("-");
	}
}

void printCenteredUnderlinedText(char* text, int spaceCountAroundText) {
	printCenteredText(text, spaceCountAroundText);
	printf("\n");
	char uderlineContainer[strlen(text)+1];
	for (int counter = 0; counter < strlen(text); counter++) {
		uderlineContainer[counter] = '-';
	}
	uderlineContainer[strlen(text)] = '\0';
	printCenteredText(uderlineContainer, spaceCountAroundText);	
}

char * returnLeftSpaceAlignedTextOfTotalLength(char *text, int totalLength) {
	int space = SPACE_BEFORE_TEXT;
	char *textToBeReturned = (char*) calloc(sizeof(char), totalLength + 2);

	for(int counter = 0; counter < space; counter++) {
			strcat(textToBeReturned, " ");
	}
	strcat(textToBeReturned, text);
	for(int counter = 0; counter < totalLength - space - strlen(text); counter++) {
		strcat(textToBeReturned, " ");
	}
	return textToBeReturned;
}

