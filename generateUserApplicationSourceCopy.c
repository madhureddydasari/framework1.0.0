#include "configure.c"

#define SETTINGS_FILE "settings.cfg"
#define PROJECT_FUNCTIONS_FILE "projectFunctions.c"
#define STRING_FUNCTIONS_FILE "stringDecorationFunctions.c"
#define MAIN_FILE "setup.c"
// #define DIR_NAME "newDomain"
#define FILE_NAME_SIZE 100
char * gPath;
char *dirName;

void createDir();
void createPath();
void generateSourceCodeCopy();
void copySourceToNewFile(char * fileName);

void main() {
	dirName = (char *) malloc(FILE_NAME_SIZE * sizeof(char));
	printf("Enter directory Name: ");
	scanf("%s", dirName);
	createDir(dirName);
	createConfigFiles();
	printf("Done!");
}


void createPath() {
	gPath = (char *) malloc(FILE_NAME_SIZE * sizeof(char));
	sprintf(gPath, "%s%s", dirName, "\\\\");
	printf("%s\n", gPath);
}

void createDir() {
	int dirStatus = mkdir(dirName);
	if (!dirStatus) {
		createPath();
		generateSourceCodeCopy();
	}
	else {
		printf("Facing problem in creating new directory or else there is an existing directory with same name.\n");
	}
}


void generateSourceCodeCopy() {
	copySourceToNewFile(SETTINGS_FILE);
	copySourceToNewFile(PROJECT_FUNCTIONS_FILE);
	copySourceToNewFile(STRING_FUNCTIONS_FILE);
	copySourceToNewFile(MAIN_FILE);
}

void copySourceToNewFile(char * fileName) {
	char *fileNameWithPath = (char*) malloc(FILE_NAME_SIZE * 5 * sizeof(char));
	sprintf(fileNameWithPath, "%s%s", gPath, fileName); 
	
	FILE *fp = fopen(fileNameWithPath, "w");
	if (fp == NULL) {
		printf("File %s is not created.\n", fileName);
	}
	else {
		printf("File %s created.\n", fileName);
		FILE *fpointer = fopen(fileName, "r");
		char *fileContent = (char *) malloc(1 * sizeof(char));

		while(!feof(fpointer)) {
			if(fread(fileContent, 1, 1, fpointer) > 0) {
				fwrite(fileContent, 1, 1, fp);
			}
		}
		fclose(fpointer);
	}
	fclose(fp);
}






